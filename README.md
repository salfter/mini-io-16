Mini-I/O 16
===========

This is a small board with 16 LEDs and pairs of switches for exercising
digital circuits.  It can draw power from the circuit under test, or it can
use USB-C.  It's intended to run at 5V, but the high-level threshold for
AHCT logic (the LEDs are buffered by two 74AHCT240s) is wide enough that it
might work at 3.3V as well.

v2.0 replaces the SMD headers with through-hole headers, as the SMD headers
tended to break off easily.
